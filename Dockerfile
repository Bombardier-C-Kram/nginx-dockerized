FROM nginx:latest
EXPOSE 80:80
COPY nginx.conf /etc/nginx/nginx.conf
COPY sites-enabled /etc/nginx/sites-enabled
COPY nginxconfig.io /etc/nginx/nginxconfig.io
